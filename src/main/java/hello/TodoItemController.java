package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@Path("todoList")
public class TodoItemController {

    @Autowired
    private TodoListRepository todoListRepository;
    @Autowired
    private TodoItemRepository todoItemRepository;

    @Path("/{id}/todoItem")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @ResponseBody
    public TodoItem create(@PathParam("id") Integer id, @QueryParam("title") String title){

        TodoList list = todoListRepository.findOne(id.longValue());

        TodoItem item = new TodoItem();

        item.setTitle(title);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        item.setCreatedAt(dateFormat.format(date));

        item.setTodoList(list);

        todoItemRepository.save(item);

        return item;
    }
}
