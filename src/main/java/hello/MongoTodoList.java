package hello;

import javax.persistence.*;
import java.util.Date;

@Entity
public class MongoTodoList {

    @Id
    public String id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String title;
    public Date createdAt;

    public MongoTodoList(){}
}
