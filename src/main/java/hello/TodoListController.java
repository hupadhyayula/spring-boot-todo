package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@Path("/todoList")
public class TodoListController {

    @Autowired
    private TodoListRepository todoListRepository;

    @POST
    public @ResponseBody String create(@QueryParam("title") String title){

        TodoList list = new TodoList();
        list.setTitle(title);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        list.setCreatedAt(dateFormat.format(date));

        todoListRepository.save(list);

        return "Created!!";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public @ResponseBody Iterable<TodoList> index(){
        return todoListRepository.findAll();
    }
}
