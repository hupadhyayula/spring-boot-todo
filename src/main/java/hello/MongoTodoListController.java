package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.Date;
import javax.ws.rs.core.MediaType;

@Component
@Path("/mongo")
public class MongoTodoListController {

    @Autowired
    private MongoTodoListReposotiry repository;

    @Path("/todoList")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public MongoTodoList create(@QueryParam("title") String title){

        MongoTodoList todoList = new MongoTodoList();
        todoList.setTitle(title);
        todoList.createdAt = new Date();

        MongoTodoList savedList = repository.save(todoList);

        return savedList;
    }
}
