package hello;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TodoList {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<TodoItem> getTodoItems() {
        return todoItems;
    }

    public void setTodoItem(TodoItem todoItem) {
        this.todoItems.add(todoItem);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    private String createdAt;

    @OneToMany(mappedBy = "todoList", fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<TodoItem> todoItems = new ArrayList<>();
}
